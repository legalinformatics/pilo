# A Public International Law Ontology (PILO) prototype

This project develops a formal ontology for data integration in the domain of public international law, especially international agreements, based on consensus terminology such as that defined in the Vienna Convention on the Law of Treaties.

It is expressed in the [OWL 2](https://www.w3.org/TR/owl2-primer/) Web Ontology Language using the [Protégé](https://protege.stanford.edu/) ontology development software with the Basic Formal Ontology [BFO-2020](https://github.com/BFO-ontology/BFO-2020), also published in [ISO/IEC 21838-2:2021](https://www.iso.org/standard/74572.html), as top-level ontology. The RDF syntax chosen is [Turtle](https://www.w3.org/TR/turtle/) for its readability, wide use and compact size, but others could be made available.

The 'core' directory contains the core version of PILO with universals/classes and relations/properties, and only a few manually added instances for convenience or illustration (e.g. the United Nations as an instance of intergovernmental organization). The 'populated' directory contains a version of PILO that was automatically populated with instance data from UNTS treaty pages and treaty texts with the [GATE](https://gate.ac.uk/) apps developed in the projects [UNTS-treatyrecords-IE](https://gitlab.com/legalinformatics/unts-treatyrecords-ie) and [treatytexts-IE](https://gitlab.com/legalinformatics/treatytexts-ie) respectively. GATE also has an ontology editor and visualization tool but it does not fully support OWL 2 (see [Chapter 14](https://gate.ac.uk/sale/tao/splitch14.html#x19-35500014) of the User Guide). The file output by GATE (and included in the 'examples' directory of the [treatytexts-IE](https://gitlab.com/legalinformatics/treatytexts-ie) project) was opened and saved again by Protégé to improve human readability, grouping relevant information together. 

More background and details will be described at https://martinakunz.gitlab.io/treaty-analytics/


